package c_instrukcje_warunkowe;

public class PrzykladIfa {
    public static void main(String[] args) {
        System.out.println("START - To się wykonuje zawsze");

        int wiek = 10;

        if (wiek >= 18){
            System.out.println("Jesteś pełnoletni, możesz wziąć kredyt");
        }

        System.out.println("KONIEC - Ten kod wykonuje się zawsze");
    }
}
