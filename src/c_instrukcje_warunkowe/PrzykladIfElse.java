package c_instrukcje_warunkowe;

public class PrzykladIfElse {
    public static void main(String[] args) {
        System.out.println("START - To się wykonuje zawsze");

        int wiek = 10;

        if (wiek >= 18){
            System.out.println("Jesteś pełnoletni, możesz wziąć kredyt");
        } else {
            System.out.println("Nie możesz wziąć kredytu");
        }

        System.out.println("KONIEC - Ten kod wykonuje się zawsze");
    }
}
