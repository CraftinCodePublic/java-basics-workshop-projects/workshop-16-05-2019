package d_stringi;

import java.util.Scanner;

public class StringZadaniePytanie {
    public static void main(String[] args) {
        System.out.println("Jak się nazywa miasto stu mostów?");
        String odpowiedz = new Scanner(System.in).nextLine();

        boolean czyOdpowiedzPoprawna = odpowiedz.equals("Wrocław");
        System.out.println("Czy odpowiedź poprawna: " + czyOdpowiedzPoprawna);
    }
}
