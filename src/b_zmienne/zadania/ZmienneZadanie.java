package b_zmienne.zadania;

public class ZmienneZadanie {
    public static void main(String[] args) {
        // ZMIENNE - ZADANIA
        //Stwórz 3 zmienne (zmiennaA, zmiennaB, zmiennaC) i przypisz im kolejno wartości 10, 20, 30
        //Wypisać wartości wszystkich zmiennych
        //Wypisać tekst - "--- Zmieniam wartości ---"
        //Zmień wcześniej utworzonym zmiennym wartości na 200, 300, 400
        //Wypisać wartości wszystkich zmiennych po zmianie wartości


        int zmiennaA = 10;
        int zmiennaB = 20;
        int zmiennaC = 30;

        System.out.println("ZmiennaA: " + zmiennaA);
        System.out.println("ZmiennaB: " + zmiennaB);
        System.out.println("ZmiennaC: " + zmiennaC);

        System.out.println("--- zmieniam wartości ----");

        zmiennaA = 200;
        zmiennaB = 300;
        zmiennaC = 400;

        System.out.println("ZmiennaA: " + zmiennaA);
        System.out.println("ZmiennaB: " + zmiennaB);
        System.out.println("ZmiennaC: " + zmiennaC);

    }
}
