package b_zmienne.zadania;

import java.util.Scanner;

public class ZadanieBoolean {
    public static void main(String[] args) {
        System.out.println("Podaj X: ");
        int x = new Scanner(System.in).nextInt();
        System.out.println("Podaj Y: ");
        int y = new Scanner(System.in).nextInt();

        boolean czyXMniejszyOd10 = x < 10; // true/false
        boolean czyYMniejszyOd10 = y < 10; // true/false

        System.out.println("Czy X mniejsze od 10: " + czyXMniejszyOd10);
        System.out.println("Czy Y mniejsze od 10: " + czyYMniejszyOd10);

        // koniunkcja - I, AND
        boolean czyObieMniejszeOd10 = x < 10 && y < 10;
        System.out.println("Czy obie mniejsze od 10: " + czyObieMniejszeOd10);

        // alternatywa - LUB, OR
        boolean czyKtorakolwiekMniejszaOd10 = x < 10 || y < 10;
        System.out.println("Czy którakolwiek mniejsza od 10: " + czyKtorakolwiekMniejszaOd10);


    }
}
