package b_zmienne.zadania;

import java.util.Scanner;

public class KalkulatorWalut {
    public static void main(String[] args) {
        // Napisz program do przeliczania kwoty podanej przez użytkownika z PLN na EUR
        // wczytaj od użytkownika liczbę typu double do zmiennej kwotaPln
        // stwórz nową zmienną kwotaEur (typu double) i przypisz do niej wynik mnożenia kwotaPln razy 4.2
        // wypisz zmienną kwotaEur na ekran


        // 1. Wypisz tekst "Podaj kwotę w PLN"
        System.out.println("Podaj kwotę w PLN: ");

        // 2. Wczytaj liczbę od użytkownika do zmiennej 'kwotaPln' (double)
        double kwotaPln = new Scanner(System.in).nextDouble();
        System.out.println("Podano kwotę PLN: " + kwotaPln);

        // 3. Stwórz zmienną 'kwotaEur' o wartości kwotaPln/4.2
        double kwotaEur = kwotaPln / 4.2;

        // 4. Wypisz wartość zmiennej 'kwotaEur'
        System.out.println("Kwota EUR: " + kwotaEur);
    }
}
