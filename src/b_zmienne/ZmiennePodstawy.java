package b_zmienne;

public class ZmiennePodstawy {
    public static void main(String[] args) {
        // Stworzenie zmiennej x typu int i przypisanie jej wartości 10
        int x = 10;
        // Stworzenie zmiennej y
        int y = 100;

        // wypisanie wartości zmiennej x
        System.out.println("Zmienna x ma wartość: " + x);
        // wypisanie wartości zmiennej y
        System.out.println("Zmienna y ma wartość: " + y);
        // wypisanie dwóch zmiennych jedną instrukcją
        System.out.println("zmienne.ZmiennePodstawy mają wartości: " + x + "," + y);
    }
}
