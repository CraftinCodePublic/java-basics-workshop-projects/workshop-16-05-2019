package b_zmienne;

import java.util.Scanner;

public class WczytywanieDanych {
    public static void main(String[] args) {
        System.out.println("Podaj wartość zmiennej: ");
        int zmiennaIntPobrana = new Scanner(System.in).nextInt();
        System.out.println("Pobrano od użytkownika wartość: " + zmiennaIntPobrana);

        System.out.println("Podaj wartość zmiennej double: ");
        double zmiennaDoublePobrana = new Scanner(System.in).nextDouble();
        System.out.println("Pobrano od użytkownika wartość: " + zmiennaDoublePobrana);
    }
}
