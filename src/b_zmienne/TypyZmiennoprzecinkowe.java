package b_zmienne;

public class TypyZmiennoprzecinkowe {
    public static void main(String[] args) {
        double x = 0.8;
        double y = 0.1;
        System.out.println("x: " + x);
        System.out.println("y: " + y);

        double z = x - y;

        System.out.println("z: " + z);
    }
}
