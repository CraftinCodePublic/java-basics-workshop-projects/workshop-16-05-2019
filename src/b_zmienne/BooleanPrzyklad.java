package b_zmienne;

import java.util.Scanner;

public class BooleanPrzyklad {
    public static void main(String[] args) {
        System.out.println("Podaj wiek:");
        int wiek = new Scanner(System.in).nextInt();

        boolean czyPelnoletni = wiek >= 18;
        boolean czyMlodszyNiz50 = wiek < 50;

        System.out.println("Czy pełnoletni: " + czyPelnoletni);
        System.out.println("Czy młodszy niż 50 lat: " + czyMlodszyNiz50);


        boolean czyPelnoletniIMlodszyNiz50Lat = czyPelnoletni && czyMlodszyNiz50;
        boolean czyPelnoletniLubMlodszyNiz50Lat = czyPelnoletni || czyMlodszyNiz50;

        System.out.println("Czy pełnoletni I młodszy niż 50 lat: " + czyPelnoletniIMlodszyNiz50Lat);
        System.out.println("Czy pełnoletni LUB młodszy niż 50 lat: " + czyPelnoletniLubMlodszyNiz50Lat);
    }
}
